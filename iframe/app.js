window.addEventListener("message", function(ev) {
  // todo: add origin check
  if (!ev.data)
    return;

  var message;

  // Register all of your domain specific callbacks here.
  var callbacks = {
    getQuotes: function(payload){
      console.log('Getting quotes');
      console.log(payload);
    }
  };

  try {
    message = JSON.parse(ev.data);
  } catch (ex) {
    console.error(ex);
  }

  // ignore messages not having a callback ID
  if (!message || !message.callbackId)
    return;

  // // we are the sender getting the callback
  if (callbacks[message.callbackId]) {
    callbacks[message.callbackId](message);
  }

  // we are the receiver so we respond with the callback ID
  // todo: restrict who can receive message (last param)
  parent.postMessage('iframeReceivedMessage', '*');
  document.getElementById('message').innerHTML = '<pre><code>' +
    JSON.stringify(message) + '</code></pre><br /><br /><br /><br />Message received and processed on ' +
    new Date();
}, false);
