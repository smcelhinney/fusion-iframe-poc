var dispatchFusionEvent = function(message) {
  var win = document.querySelector("iframe").contentWindow;
  switch (message) {
    case 'getQuotes':
      var booking = {
        id: 12739123,
        search: '2015-01-11',
        name: 'Foo'
      };
      win.postMessage(JSON.stringify(Object.assign({}, {
        callbackId: 'getQuotes'
      }, booking)), "*");
      break;
    case 'drawerOpened':
      console.log('Drawer opened, work your magic');
      break;
    default:
      console.log(message);
      break;
  };
};


window.addEventListener("message", function(ev) {
  if (ev.data === 'iframeReceivedMessage') {
    document.getElementById('response').innerHTML = 'Response received' + new Date();

    // Now we would do something like,
    // a) open the drawer via a Promise and
    // b) when the promise resolves, dispatch another event to notify fusion
    dispatchFusionEvent('drawerOpened');

  }
}, false);
